package com.cloud.schema.controller;

import com.alibaba.fastjson.JSONObject;
import com.cloud.schema.bo.MessageBO;
import com.cloud.schema.utils.QRCodeUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;


@RestController
@RequestMapping("file")
public class FileController {
    private final String SUCCESS = "success";

    @PostMapping(value = "/upload1")
    public String upload(@RequestParam("uploadFile") MultipartFile file,MessageBO message) {
        String filename=file.getOriginalFilename();
        String trueName = filename.substring(0, filename.indexOf("."));

//        String md5Str = SecureUtil.md5(content);

        System.out.println(message);
        System.out.println(message.getContent());
        return SUCCESS;
    }

    @PostMapping(value = "/message")
    public String message(@RequestBody  MessageBO message) {
        System.out.println(message);
        System.out.println(message.getContent());
        return SUCCESS;
    }

    @PostMapping(value = "/upload")
    public String assignCard(HttpServletRequest request) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile file = multipartRequest.getFile("file");// 获取上传文件对象
        System.out.println(file.getOriginalFilename());
        System.out.println(file.getName());

        try(InputStream is = file.getInputStream()) {
            String code= QRCodeUtils.codeAnalyze(is);
            System.out.println("二维码读取成功：   "+code);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String jsonType = request.getParameter("message");
        MessageBO messageBO = JSONObject.parseObject(jsonType, MessageBO.class);
        System.out.println(messageBO);
        return  SUCCESS;
    }


}
