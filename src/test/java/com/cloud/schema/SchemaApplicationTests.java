package com.cloud.schema;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.DES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Base64;


class SchemaApplicationTests {

    @Test
    void contextLoads() {
        String filename = "wrtwa我的 >? 人份额啊 的.jpg";
        String trueName = filename.substring(0, filename.indexOf("."));
        System.out.println(trueName);
        String md5Str = SecureUtil.md5(trueName);
        System.out.println(md5Str);
        System.out.println("85639bc3aa7ecb2f4bccab84a2dffaf1");

    }

    private  String encrypt(String content){
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.DES.getValue()).getEncoded();
        DES des = SecureUtil.des(key);
        String encryptHex = des.encryptHex(content);
        return  encryptHex;
    }

    private  String decrypt(String content){
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.DES.getValue()).getEncoded();
        DES des = SecureUtil.des(key);
        String decryptStr = des.decryptStr(content);
        return  decryptStr;
    }





}
