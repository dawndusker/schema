package com.cloud.schema.bo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MessageBO {
    private String collectorId;
    private String content;
    private String dataSource;
    private String fileMd5;
    private String filename;
    private String filesize;
    private String id;
    private String machineId;
    private String type;
    private  Long captureTimestamp;
    private  Long timestamp;
}
